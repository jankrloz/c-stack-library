struct node{
    char key; //representa el elemento que se guardar� en la pila.
    struct node *next; //Ligado din�mico de los elementos de la pila, es un apuntador a una estructura que forma parte
};//Estructura que representara los elementos de la pila y como ser�n ligadas.

//Nicknames

typedef struct node NODE; //Alias para el nodo.
typedef NODE * Stack; //Alias para el apuntador a la cima, controlar� la pila.
typedef enum boolean {FALSE, TRUE} boolean; //Enumeraci�n creada para identificar los valores logicos.
typedef enum msg {NO_MEMORY, OK} message; //Enumeracion creada para identificar valores logicos asociados a la reservacion de memoria


//Prototypes

NODE topStack (Stack);              //Apuntador simple
/*
Regresa una copia del ultimo elemento (estructura) agregado a la pila sin modificarla
como parametro recibe el apuntador controlador de la pila
*/

NODE popStack (Stack *);            //Doble apuntador
/*
Regresa el ultimo elemento (estructura) agregado a la pila, modificando el apuntador controlador y liberando memoria
como parametro recibe un doble apuntador al controlador de la pila
*/

message pushStack (Stack *, NODE);  //Doble apuntador, estructura NODE
/*
Modifica la pila agregando un nuevo elemento (estructura), realiza la asignacion de memoria para este
y retorna un valor logico relacionado al enumerador especifico para su funcion:
          0 si la asignacion de memoria no fue correcta (NO_MEMORY)
          1 si la asignacion de memoria fue exitosa (OK)
como parametros recibe un doble apuntador al controlador de la pila y el elemento a a�adir a la pila (estructura)
*/

boolean emptyStack (Stack);         //Apuntador simple
/*
Regresa un valor logico relacionado a una aenumeracion especifica para su funcion
          0 si el apuntador controlador es a un elemento de la pila (pila no vacia) (FALSE)
          1 si el apuntador controlador es a nulo (pila vacia) (TRUE)
recibe como parametro el apuntador controlador de la pila
*/

void createStack (Stack *);         //Doble apuntador
/*
Realiza las asignaciones necesarias para el primer uso de la pila, en este caso, asigna el apuntador controlador
de la pila a nulo, recibe como parametro un doble apuntador al controlador de la pila
no tiene retorno
*/

void eraseStack(Stack *);           //Doble apuntador
/*
Mediante el uso de la funcion popStack, realiza la eliminacion de los elementos en la pila
recibe como parametro un doble apuntador al controlador de la pila
no tiene retorno
*/

