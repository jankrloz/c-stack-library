#include <stdio.h>
#include <stdlib.h>
#include "stack.h"

int precedencia (char inf, char key);

int precedencia (char inf, char key){
    int a, b;
    if (inf == '+' || inf == '-')
        a = 1;
    if (inf == '*' || inf == '/')
        a = 2;
    if (key == '+' || key == '-')
        b = 1;
    if (key == '*' || key == '/')
        b = 2;
    if (a > b)
        return 1;
    else
        return 0;
}

int main()
{
  NODE aux;
  Stack opr;
  int i, j;
  char inf[50];
  char pos[50];
  createStack (&opr);
  printf ("Ingrese la expresion a calcular: "); gets (inf);
  i = 0; j = 0;
  while (inf[i] !='\0'){
        if (isdigit(inf[i])) {
            pos [j++] = inf [i++];
        }
        else if (inf[i] == '+' || inf[i] == '-' || inf[i] == '*' || inf[i] == '/'){
            if (emptyStack(opr) || inf [i] == '(' || inf [i] == '[' || inf [i] == '{'){
                aux.key = inf [i++];
                pushStack (&opr, aux); 
            }
            else {
                aux = topStack (opr);
                if (precedencia (inf[i], aux.key)){
                    aux.key = inf [i++];
                    pushStack (&opr, aux); 
                }
                else{
                    aux = popStack (&opr);
                    pos [j++] = aux.key;
                    while (!emptyStack (opr) && !precedencia(inf[i], topStack(opr).key)){
                        aux = popStack (&opr);
                        pos [j++] = aux.key;
                    }
                    aux.key = inf[i++];
                    pushStack (&opr, aux);
                }
            }
        }
        
    }
    while (!emptyStack(opr)){
        aux = popStack(&opr);
        pos [j++] = aux.key;
    }
    pos[j] = '\0';
    puts (pos);
    system("PAUSE");       	
    return EXIT_SUCCESS;
}
