#include <stdlib.h>
#include "stack.h"

NODE topStack (Stack p){
    NODE aux; //estructura auxiliar
    aux = *p; //Apuntador controlador de la pila al primer elemento
    aux.next = NULL; // el campo "next" del auxiliar no es necesario
    return aux; //devuelve la estructura
}

NODE popStack (Stack *p){
    NODE copy; //estructura que servira para copiar el ultimo elemento
    Stack aux; //apuntador a estructura auxiliar
    copy = **p; //asignacion de la cima de la pila a la copia
    copy.next = NULL; // el campo "next" de la copia no es necesario
    aux = (*p)->next; //el auxiliar guardara una copia del apuntador al siguiente elemento
    free (*p); //liberamos el ultimo elemento
    *p = aux;  //movemos el apuntador controlador a la nueva cima (elemento siguiente)
    return copy; //devuelve la copia de la cima anterior
}

message pushStack (Stack *p, NODE n){
    Stack temp; //creamos un apuntador a estructura auxiliar
    temp = malloc(sizeof(NODE)); //reservamos espacio necesario para un nodo
    if (temp == NULL)
        return NO_MEMORY; //Checamos que el malloc se realizo correctamente
    *temp = n; //copiamos el nodo a agregar a la pila, al espacio reservado
    temp->next = *p; //ligamos el nuevo elemento con ahora, el elemento siguiente (cima anterior)
    *p = temp; //movemos el apuntador controlador a la nueva cima
    return OK;  //regresamos el mensaje OK
}

boolean emptyStack (Stack p){
    if (p == NULL){
            return TRUE;//checa si existen elementos o no en la pila
    }
    return FALSE;
}

void createStack (Stack *p){
    *p = NULL;//asigna el apuntador controlador de la pila a nulo
}

void eraseStack (Stack *p){
    while (!emptyStack (*p)){
        popStack (p);//hace los pops necesarios para vaciar la pila
    }
}
